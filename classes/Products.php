<?php

class Products
{

    public $products;

    public function __construct()
    {
        $this->setProducts();
    }

    public function getProduct($id)
    {
        return $this->products[$id];
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setProducts()
    {
        $products[] = [
            'name' => 'Mercedes G3',
            'desc' => 'Mercedes-Benz G3 is a mid-size luxury car produced by the German automaker Mercedes-Benz. It was
                    introduced in the United States in 2001 and was the first car to be produced by the company.
                    The G3 was the first car to be produced by the company, and was the first car to be produced in
                    the United States. The G3 was the first car to be produced in the United States.',
            'price' => '120 000',
            'speed' => '320KMH',
            'accel' => '4,75s',
            'power' => '300',
            'image' => './assets/images/mercedesg3.png',
            'color' => '#B32F3F',
        ];

        $products[] = [
            'name' => 'Mercedes G3',
            'desc' => 'Mercedes-Benz G3 is a mid-size luxury car produced by the German automaker Mercedes-Benz. It was
                    introduced in the United States in 2001 and was the first car to be produced by the company.
                    The G3 was the first car to be produced by the company, and was the first car to be produced in
                    the United States. The G3 was the first car to be produced in the United States.',
            'price' => '120 000',
            'speed' => '320KMH',
            'accel' => '4,75s',
            'power' => '300',
            'image' => './assets/images/mercedesg3.png',
            'color' => '#B32F3F',
        ];

        $this->products = $products;
    }
}
