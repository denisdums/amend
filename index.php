<!doctype html>
<html lang="fr" x-data="{ dark: false }">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Amend</title>
    <link rel="icon" href="./assets/images/icon.png">
    <!-- Styles -->
    <template x-if="dark">
        <link rel="stylesheet" href="./dist/css/app-dark.css">
    </template>
    <template x-if="!dark">
        <link rel="stylesheet" href="./dist/css/app.css">
    </template>

    <!-- Script -->
    <script src="./dist/js/pricing.js"></script>
    <script src="./dist/js/app.js" defer></script>
</head>
<body>
<main x-data="{ screen: 'home', currency: '€' }">
    <?php
    include 'partials/newsletter.php';
    include 'partials/header.php';
    include 'partials/home.php';
    include 'partials/shop.php';
    include 'partials/about.php';
    include 'partials/contact.php';
    ?>
</main>
</body>
</html>