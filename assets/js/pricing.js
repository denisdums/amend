import $ from 'jquery';
import Alpine from 'alpinejs'

window.getPrice = function () {
    return {
        async getPrice(price, sign) {
            let currency = getCurrency(sign);
            let finalPrice = await getStatus(price, currency)
            return finalPrice
        }
    }
}

function fetchStatus() {
    let request = 'https://api.factmaven.com/xml-to-json/?xml=https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml?295c8f6c199db5284bbddd1f612471e2'
    return fetch(request)
        .then(response => response.json())
        .then((jsondata) => { return jsondata.Envelope.Cube.Cube.Cube })
}

function getStatus(price, currency) {
    price = parseInt(price);
    return fetchStatus().then((response) => {
        let index = response.findIndex(item => item.currency == currency);
        if (index === -1){
            return price;
        }else {
            let rate = response[index].rate;
            let result = (price * rate);
            if (result - Math.floor(result) > 1){
                return result;
            }else {
                return parseFloat(result.toFixed(2))
            }

        }
    });
}

function getCurrency(sign){
    let relatedSigns = {
        '$': 'USD',
        '€': 'EUR',
        '¥': 'JPY',
    }
    return relatedSigns[sign];
}
