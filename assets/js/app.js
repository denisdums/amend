import $ from 'jquery';
import 'slick-carousel';
import Alpine from 'alpinejs'
import { Fancybox, Carousel, Panzoom } from "@fancyapps/ui";


window.Alpine = Alpine

Alpine.store('newsletter', {
    status: false,

    activate() {
        this.status = true;
    },

    deactivate() {
        this.status = false;
    }
})


Alpine.start()

$(document).ready(function () {
    $('#home-slider').slick({
        arrows: false,
        dots: true,
        infinite: false,
        speed: 200,
    })
})

$(document).mouseleave(function () {
    Alpine.store('newsletter').activate();
});