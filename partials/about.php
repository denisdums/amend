<section class="about" x-show="screen === 'about'" x-transition:enter.duration.500ms>
    <div class="container">
        <div class="small-container">
            <h2 class="title">A propos</h2>
            <div class="custom-flex">
                <div class="custom-flex_column">
                    <p class="text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat tellus tincidunt lobortis mattis iaculis senectus ultrices sit in. Pretium adipiscing eleifend risus rhoncus est sed risus. Consectetur vulputate elit porttitor egestas mi volutpat, amet. Diam, neque venenatis, feugiat aliquam. Massa condimentum a fames nunc porta quis adipiscing. At etiam faucibus et aliquam eu habitant. Aliquet feugiat viverra egestas non aenean id elementum augue. Lectus tristique sed diam, faucibus. Sit magna facilisi purus a, tincidunt viverra. Sodales a commodo enim et ultricies nullam blandit id. Amet egestas accumsan faucibus in adipiscing.
                        <br><br>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat tellus tincidunt lobortis mattis iaculis senectus ultrices sit in. Pretium adipiscing eleifend risus rhoncus est sed risus. Consectetur vulputate elit porttitor egestas mi volutpat, amet.
                    </p>
                </div>
                <div class="custom-flex_column">
                    <a href="assets/images/voiture1.png" data-fancybox="gallery-about" class="img-responsive">
                        <img src="assets/images/voiture1.png" aria-hidden="true">
                    </a>
                    <a href="assets/images/voiture2.png" data-fancybox="gallery-about" class="img-responsive">
                        <img src="assets/images/voiture2.png" aria-hidden="true">
                    </a>
                    <a href="assets/images/voiture3.png" data-fancybox="gallery-about" class="img-responsive">
                        <img src="assets/images/voiture3.png" aria-hidden="true">
                    </a>
                    <a href="assets/images/voiture4.png" data-fancybox="gallery-about" class="img-responsive">
                        <img src="assets/images/voiture4.png" aria-hidden="true">
                    </a>
                    <a href="assets/images/voiture5.png" data-fancybox="gallery-about" class="img-responsive">
                        <img src="assets/images/voiture5.png" aria-hidden="true">
                    </a>
                    <a href="assets/images/voiture6.png" data-fancybox="gallery-about" class="img-responsive">
                        <img src="assets/images/voiture6.png" aria-hidden="true">
                    </a>
                    <a href="assets/images/voiture7.png" data-fancybox="gallery-about" class="img-responsive">
                        <img src="assets/images/voiture7.png" aria-hidden="true">
                    </a>
                    <a href="assets/images/voiture8.png" data-fancybox="gallery-about" class="img-responsive">
                        <img src="assets/images/voiture8.png" aria-hidden="true">
                    </a>
                    <a href="assets/images/voiture9.png" data-fancybox="gallery-about" class="img-responsive">
                        <img src="assets/images/voiture9.png" aria-hidden="true">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
