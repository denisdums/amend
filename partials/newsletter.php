<div class="newsletter" x-show="$store.newsletter.status">
    <div class="newsletter-content" @click.away="$store.newsletter.deactivate()">
        <h2 class="title">Newsletter</h2>
        <p class="text">Tu n'est pas encore inscrit à la newsletter ? Alors inscrit toi de suite !</p>
        <form class="custom-flex">
            <div class="custom-flex_column">
                <label for="newsletter-email">Email</label></label>
                <input type="email" name="email"  required>
            </div>
            <div class="custom-flex_column">
                <button class="button" type="submit">S'inscrire</button>
            </div>
        </form>
    </div>
</div>