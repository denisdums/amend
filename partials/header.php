<header x-data="{ open: false }">
    <div class="container">
        <div class="logo">
            <template x-if="dark">
                <img src="./assets/images/AmendWhite.png" alt="Logo du site Amend" @click.prevent="screen = 'home'">
            </template>
            <template x-if="!dark">
                <img src="./assets/images/AmendBlack.png" alt="Logo du site Amend" @click.prevent="screen = 'home'">
            </template>
        </div>

        <nav :class="{ 'open': open === true }">
            <ul>
                <li>
                    <button class="text" :class="{ 'active': screen === 'home' }"
                            @click.prevent="screen = 'home'; open = !open">Accueil
                    </button>
                </li>
                <li>
                    <button class="text" :class="{ 'active': screen === 'shop' }"
                            @click.prevent="screen = 'shop'; open = !open">Boutique
                    </button>
                </li>
                <li>
                    <button class="text" :class="{ 'active': screen === 'about' }"
                            @click.prevent="screen = 'about'; open = !open">A propos
                    </button>
                </li>
                <li>
                    <button class="text" :class="{ 'active': screen === 'contact' }"
                            @click.prevent="screen = 'contact'; open = !open">Contact
                    </button>
                </li>
                <li>
                    <div class="currency" x-data="{ display: false }">
                        <button x-text="currency" @click.prevent="display = !display"></button>
                        <ul x-show="display">
                            <li x-data="{localCurrency: '€'}">
                                <button x-text="localCurrency" @click.away="display = false"
                                        :class="{'active' : localCurrency === currency}"
                                        @click.prevent="currency = localCurrency"></button>
                            </li>
                            <li x-data="{localCurrency: '$'}">
                                <button x-text="localCurrency" @click.prevent="currency = localCurrency"></button>
                            </li>
                            <li x-data="{localCurrency: '¥'}">
                                <button x-text="localCurrency" @click.prevent="currency = localCurrency"></button>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <button class="text modeBtn" @click.prevent="dark = !dark"
                            :class="{ 'Darkmode': dark === true,'Lightmode':  dark != true}"
                    </button>
                </li>
            </ul>
        </nav>
        <button type="button" class="burger-button" data-toggle="collapse" aria-label="Navigation mobile"
                :class="{ 'open': open === true }" @click.prevent="open = !open">
            <span class="one"></span>
            <span class="two"></span>
            <span class="three"></span>
        </button>
    </div>
</header>