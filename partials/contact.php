<section x-show="screen === 'contact'" x-transition:enter.duration.500ms x-data="{selectedProduct: false,togglePopin: false}">
    <div class="container">
        <div class="small-container contact">
            <h1 class="title">Contact</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum excepturi exercitationem fugiat laborum nisi possimus suscipit? Aut blanditiis delectus exercitationem minima provident. Dignissimos dolores dolorum nesciunt quasi ratione sed voluptatibus.</p>

            <div class="contact__content">
                <?php include 'form.php'; ?>
                <div class="contact__content__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.92909824098!2d2.34889831501058!3d48.8583779792409!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sParis%2C+France!5e0!3m2!1sfr!2sfr!4v1559242859095!5m2!1sfr!2sfr" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

</section>