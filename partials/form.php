<div class="contact__content__form" x-data="{send: false}">
    <form>
        <div class="contact__content__form__row" x-show="togglePopin === true">
            <label for="produit">Produit</label>
            <input type="text" id="produit" x-bind:value="selectedProduct.title" disabled/>
        </div>
        <div class="contact__content__form__row double">
            <div>
                <label for="nom">Nom</label>
                <input type="text" id="nom" />
            </div>
            <div>
                <label for="prenom">Prénom</label>
                <input type="text" id="prenom" />
            </div>
        </div>
        <div class="contact__content__form__row">
            <label for="email">Email</label>
            <input type="email" id="email" />
        </div>
        <div class="contact__content__form__row">
            <label for="message">Message</label>
            <textarea name="message" id="message" cols="30" rows="10"></textarea>
        </div>
    </form>
    <button id="sendContact" class="button" x-bind:disabled="send" @click.prevent="send = true" x-text="send ? 'Message Envoyé' : 'Envoyer'"></button>
</div>