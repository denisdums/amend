<section class="shop" x-show="screen === 'shop'" x-data="{ display: 'mercedesG3', togglePopin: false, selectedProduct: {} }" x-transition:enter.duration.500ms>
    <div class="container" x-data="getPrice()">
        <div class="small-container">
            <nav>
                <ul>
                    <li>
                        <button @click.prevent="display = 'mercedesG3'" :class="{'active' : display === 'mercedesG3'}">
                            Mercedes G3
                        </button>
                    </li>
                    <li>
                        <button @click.prevent="display = 'mercedesG4'" :class="{'active' : display === 'mercedesG4'}">
                            Mercedes G4
                        </button>
                    </li>
                    <li>
                        <button @click.prevent="display = 'mercedesG5'" :class="{'active' : display === 'mercedesG5'}">
                            Mercedes G5
                        </button>
                    </li>
                    <li>
                        <button @click.prevent="display = 'mercedesG6'" :class="{'active' : display === 'mercedesG6'}">
                            Mercedes G6
                        </button>
                    </li>
                </ul>
            </nav>

            <div class="popin" x-show="togglePopin === true">
                <div class="popin-content" @click.away="togglePopin =  false">
                    <h2 class="title" x-text="selectedProduct.title"></h2>
                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat tellus tincidunt lobortis mattis iaculis senectus ultrices sit in.</p>
                    <?php include 'form.php'; ?>
                </div>
            </div>

            <article x-data="{ product: { price: '120000', title: 'Mercedes G3', slug: 'mercedesG3'}}" class="article"
                     x-show="display === product.slug">
                <h3 class="article--title" x-text="product.title"></h3>
                <img class="img-responsive" src="assets/images/mercedesg3.png" alt="" aria-hidden="true">
                <p class="text">
                    Mercedes-Benz G3 is a mid-size luxury car produced by the German automaker Mercedes-Benz. It was
                    introduced in the United States in 2001 and was the first car to be produced by the company.
                    The G3 was the first car to be produced by the company, and was the first car to be produced in
                    the United States. The G3 was the first car to be produced in the United States.
                </p>
                <table>
                    <tbody>
                    <tr>
                        <td>Vitesse Maximale</td>
                        <td>320KMH</td>
                    </tr>
                    <tr>
                        <td>0-100KMH</td>
                        <td>4,75s</td>
                    </tr>
                    <tr>
                        <td>Puissance</td>
                        <td>300 CH</td>
                    </tr>
                    <tr>
                        <td>Prix</td>
                        <td><span x-text="await getPrice(product.price, currency)"></span><small x-text="currency"></small></td>
                    </tr>
                    </tbody>
                </table>
                <button class="button" @click="selectedProduct = product; togglePopin = true">Acheter</button>
            </article>
            <article x-data="{ product: { price: '180000', title: 'Mercedes G4', slug: 'mercedesG4'}}" class="article"
                     x-show="display === product.slug">
                <h3 class="article--title" x-text="product.title"></h3>
                <img class="img-responsive" src="assets/images/mercedesg4.png" alt="" aria-hidden="true">
                <p class="text">
                    Mercedes-Benz G4 is a mid-size luxury car produced by the German automaker Mercedes-Benz. It was
                    introduced in the United States in 2001 and was the first car to be produced by the company.
                    The G3 was the first car to be produced by the company, and was the first car to be produced in
                    the United States. The G3 was the first car to be produced in the United States.
                </p>
                <table>
                    <tbody>
                    <tr>
                        <td>Vitesse Maximale</td>
                        <td>320KMH</td>
                    </tr>
                    <tr>
                        <td>0-100KMH</td>
                        <td>4,75s</td>
                    </tr>
                    <tr>
                        <td>Puissance</td>
                        <td>300 CH</td>
                    </tr>
                    <tr>
                        <td>Prix</td>
                        <td><span x-text="await getPrice(product.price, currency)"></span><small x-text="currency"></small></td>
                    </tr>
                    </tbody>
                </table>
                <button class="button" @click="selectedProduct = product; togglePopin = true">Acheter</button>
            </article>
            <article x-data="{ product: { price: '90000', title: 'Mercedes G5', slug: 'mercedesG5'}}" class="article"
                     x-show="display === product.slug">
                <h3 class="article--title" x-text="product.title"></h3>
                <img class="img-responsive" src="assets/images/mercedesg5.png" alt="" aria-hidden="true">
                <p class="text">
                    Mercedes-Benz G5 is a mid-size luxury car produced by the German automaker Mercedes-Benz. It was
                    introduced in the United States in 2001 and was the first car to be produced by the company.
                    The G3 was the first car to be produced by the company, and was the first car to be produced in
                    the United States. The G3 was the first car to be produced in the United States.
                </p>
                <table>
                    <tbody>
                    <tr>
                        <td>Vitesse Maximale</td>
                        <td>320KMH</td>
                    </tr>
                    <tr>
                        <td>0-100KMH</td>
                        <td>4,75s</td>
                    </tr>
                    <tr>
                        <td>Puissance</td>
                        <td>300 CH</td>
                    </tr>
                    <tr>
                        <td>Prix</td>
                        <td><span x-text="await getPrice(product.price, currency)"></span><small x-text="currency"></small></td>
                    </tr>
                    </tbody>
                </table>
                <button class="button" @click="selectedProduct = product; togglePopin = true">Acheter</button>
            </article>
            <article x-data="{ product: { price: '170000', title: 'Mercedes G6', slug: 'mercedesG6'}}" class="article"
                     x-show="display === product.slug">
                <h3 class="article--title" x-text="product.title"></h3>
                <img class="img-responsive" src="assets/images/mercedesg6.png" alt="" aria-hidden="true">
                <p class="text">
                    Mercedes-Benz G6 is a mid-size luxury car produced by the German automaker Mercedes-Benz. It was
                    introduced in the United States in 2001 and was the first car to be produced by the company.
                    The G3 was the first car to be produced by the company, and was the first car to be produced in
                    the United States. The G3 was the first car to be produced in the United States.
                </p>
                <table>
                    <tbody>
                    <tr>
                        <td>Vitesse Maximale</td>
                        <td>320KMH</td>
                    </tr>
                    <tr>
                        <td>0-100KMH</td>
                        <td>4,75s</td>
                    </tr>
                    <tr>
                        <td>Puissance</td>
                        <td>300 CH</td>
                    </tr>
                    <tr>
                        <td>Prix</td>
                        <td><span x-text="await getPrice(product.price, currency)"></span><small x-text="currency"></small></td>
                    </tr>
                    </tbody>
                </table>
                <button class="button" @click="selectedProduct = product; togglePopin = true">Acheter</button>
            </article>
        </div>
    </div>
</section>