<section x-show="screen === 'home'" x-transition:enter.duration.500ms>
    <div id="home-slider">
        <div class="home-slider-item">
            <div class="container small-container">
                <div class="home-slider-item-image">
                    <a href="./assets/images/mercedesg3.png" class="fancybox" data-fancybox="single">
                        <img src="./assets/images/mercedesg3.png" alt="image de la voiture mercedes g3">
                    </a>
                    <div class="shape">
                        <?php include 'partials/shape.php' ?>
                    </div>
                </div>
                <div>
                    <div class="title">Mercedes G3</div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat tellus tincidunt lobortis mattis
                        iaculis senectus ultrices sit in. Pretium adipiscing eleifend risus rhoncus est sed risus.
                        Consectetur vulputate elit porttitor egestas mi volutpat, amet. Diam, neque venenatis, feugiat
                        aliquam. Massa condimentum a fames nunc porta quis adipiscing. At etiam faucibus et aliquam eu
                        habitant. Aliquet feugiat viverra egestas non aenean id elementum augue. Lectus tristique sed
                        diam, faucibus. Sit magna facilisi purus a, tincidunt viverra. Sodales a commodo enim et
                        ultricies nullam blandit id. Amet egestas accumsan faucibus in adipiscing.</p>
                </div>
            </div>
        </div>
        <div class="home-slider-item">
            <div class="container small-container">
                <div class="home-slider-item-image">
                    <a href="./assets/images/mercedesg4.png" class="fancybox" data-fancybox="single">
                        <img src="./assets/images/mercedesg4.png" alt="image de la voiture mercedes g3">
                    </a>
                    <div class="shape">
                        <?php include 'partials/shape.php' ?>
                    </div>
                </div>
                <div>
                    <div class="title">Mercedes G4</div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat tellus tincidunt lobortis mattis
                        iaculis senectus ultrices sit in. Pretium adipiscing eleifend risus rhoncus est sed risus.
                        Consectetur vulputate elit porttitor egestas mi volutpat, amet. Diam, neque venenatis, feugiat
                        aliquam. Massa condimentum a fames nunc porta quis adipiscing. At etiam faucibus et aliquam eu
                        habitant. Aliquet feugiat viverra egestas non aenean id elementum augue. Lectus tristique sed
                        diam, faucibus. Sit magna facilisi purus a, tincidunt viverra. Sodales a commodo enim et
                        ultricies nullam blandit id. Amet egestas accumsan faucibus in adipiscing.</p>
                </div>
            </div>
        </div>
        <div class="home-slider-item">
            <div class="container small-container">
                <div class="home-slider-item-image">
                    <a href="./assets/images/mercedesg5.png" class="fancybox" data-fancybox="single">
                        <img src="./assets/images/mercedesg5.png" alt="image de la voiture mercedes g3">
                    </a>
                    <div class="shape">
                        <?php include 'partials/shape.php' ?>
                    </div>
                </div>
                <div>
                    <div class="title">Mercedes G5</div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat tellus tincidunt lobortis mattis
                        iaculis senectus ultrices sit in. Pretium adipiscing eleifend risus rhoncus est sed risus.
                        Consectetur vulputate elit porttitor egestas mi volutpat, amet. Diam, neque venenatis, feugiat
                        aliquam. Massa condimentum a fames nunc porta quis adipiscing. At etiam faucibus et aliquam eu
                        habitant. Aliquet feugiat viverra egestas non aenean id elementum augue. Lectus tristique sed
                        diam, faucibus. Sit magna facilisi purus a, tincidunt viverra. Sodales a commodo enim et
                        ultricies nullam blandit id. Amet egestas accumsan faucibus in adipiscing.</p>
                </div>
            </div>
        </div>
        <div class="home-slider-item">
            <div class="container small-container">
                <div class="home-slider-item-image">
                    <a href="./assets/images/mercedesg6.png" class="fancybox" data-fancybox="single">
                        <img src="./assets/images/mercedesg6.png" alt="image de la voiture mercedes g3">
                    </a>
                    <div class="shape">
                        <?php include 'partials/shape.php' ?>
                    </div>
                </div>
                <div>
                    <div class="title">Mercedes G6</div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat tellus tincidunt lobortis mattis
                        iaculis senectus ultrices sit in. Pretium adipiscing eleifend risus rhoncus est sed risus.
                        Consectetur vulputate elit porttitor egestas mi volutpat, amet. Diam, neque venenatis, feugiat
                        aliquam. Massa condimentum a fames nunc porta quis adipiscing. At etiam faucibus et aliquam eu
                        habitant. Aliquet feugiat viverra egestas non aenean id elementum augue. Lectus tristique sed
                        diam, faucibus. Sit magna facilisi purus a, tincidunt viverra. Sodales a commodo enim et
                        ultricies nullam blandit id. Amet egestas accumsan faucibus in adipiscing.</p>
                </div>
            </div>
        </div>
    </div>
</section>